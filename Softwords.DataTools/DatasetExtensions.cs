﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Softwords.DataTools
{
    public static class DatasetExtensions
    {
        public static void NameTables(this DataSet ds, string[] tablenames)
        {
            for (int i = 0; i < tablenames.Length; i++)
            {
                ds.Tables[i].TableName = tablenames[i];
            }
        }
        public static Dictionary<string, object> ToParentChild(this DataSet ds, string[] relatedTableNames)
        {
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
            {
                return new Dictionary<string, object>();    
            }
            DataRow parent = dt.Rows[0];
            Dictionary<string, object> output = parent.ToDictionary();
            // now add the related tables
            for(int i = 0; i < relatedTableNames.Length; i++)
            {
                ds.Tables[i + 1].TableName = relatedTableNames[i];          // let the table get this name too
                output.Add(relatedTableNames[i], ds.Tables[i + 1]);         // ignoring the first record set, already used for the base
            }
            return output;
        }
        public static Dictionary<string, object> ToParentChild(this DataSet ds, string[] relatedTableNames, StructureOptions[] options)
        {
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
            {
                return new Dictionary<string, object>();
            }
            DataRow parent = dt.Rows[0];

            Dictionary<string, object> output = parent.ToDictionary();
            // now add the related tables
            for (int i = 0; i < relatedTableNames.Length; i++)
            {
                dt = ds.Tables[i + 1];
                string tablename = relatedTableNames[i];
                switch (options[i])
                {
                    case StructureOptions.One:
                        // one to one - grab only the first record if there is one
                        if (dt.Rows.Count == 0)
                        {
                            output.Add(tablename, null);
                        }
                        else
                        {
                            output.Add(tablename, dt.Rows[0].ToDictionary());
                        };
                        break;
                    case StructureOptions.Many:
                        output.Add(tablename, ds.Tables[i + 1]);         // ignoring the first record set, already used for the base
                        break;
                }
            }
            return output;
        }
        public static Dictionary<string, object> ToDictionary(this DataRow dr)
        {
            Dictionary<string, object> output = new Dictionary<string, object>();
            foreach (DataColumn f in dr.Table.Columns)
            {
                output.Add(f.ColumnName, dr.Field<object>(f));
            }
            return output;

        }
    }
}
