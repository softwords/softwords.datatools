﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Softwords.DataTools
{
    public abstract class DSBase
    {
        protected DSBase()
        {
            Server = string.Empty;
            Database = string.Empty;
        }
        protected DSBase(string server, string database)
            : this()
        {
          Server = server ?? string.Empty;
          Database = database ?? string.Empty;
        }
        protected DSBase(IDBDefn fltr)
        {
            Server = fltr.Server ?? string.Empty;
            Database = fltr.Database ?? string.Empty;
        }

        protected string Server { get; set; }
        protected string Database { get; set; }

        #region Private

        protected abstract DataSet cmdToDataset(SqlCommand cmd);
        protected abstract DataSet cmdToDataset(SqlCommand cmd, bool keepContext);


        protected IDataResult sqlExec(SqlCommand cmd)
        {
            return sqlExec(cmd, true);
        }
        protected IDataResult sqlExec(SqlCommand cmd, bool hasPageInfo)
        {

            DataSet ds = cmdToDataset(cmd);
            return new DataResult(ds, hasPageInfo);
        }
      protected IDataResult sqlExec(SqlCommand cmd, bool hasPageInfo, string tag)
      {

      DataSet ds = cmdToDataset(cmd);
      return new DataResult(ds, hasPageInfo, tag);


    }


    #endregion

  }
}
