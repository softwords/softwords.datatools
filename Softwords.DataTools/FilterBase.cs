﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.InteropServices;
using adoNet = System.Data;
using System.ComponentModel;    // for InotifyPropertychanged
using System.Xml.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Softwords.DataTools
{
    #region Filter Parameters


    [ComVisible(true)]
    [Guid("E73E9420-0013-402C-A3B8-05227AECF537")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IFilterParam
    {
        [DispId(0)]
        object Value { get; set; }


        void Set(object newvalue);

        string Name { get; }
        bool Locked { get; set; }
        void Clear();
    }


    [ComVisible(true)]
    [Guid("017C3C1F-77A4-4ACD-A4E7-946F60BD2D8C")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IParamToChangeEventArgs
    {
        [DispId(1)]
        FilterParam FilterParam { get; }

        [DispId(0)]
        object NewValue { get; }

        [DispId(2)]
        bool cancel { get; set; }

    }

    [ComVisible(true)]
    [Guid("D0AA19F4-D779-4D6D-A011-F7EE3A015084")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IParamToChangeEventArgs))]

    public class ParamToChangeEventArgs : EventArgs, IParamToChangeEventArgs
    {
        public ParamToChangeEventArgs(FilterParam param, object newValue)
        {
            filterparam = param;
            newvalue = newValue;
            cancel = false;
        }

        // Summary:
        //     Gets the name of the property that changed.
        //
        // Returns:
        //     The name of the property that changed.


        public FilterParam FilterParam { get { return filterparam; } }

        [DispId(0)]
        public object NewValue { get { return newvalue; } }
        public virtual bool cancel { get; set; }

        private FilterParam filterparam;
        object newvalue;
    }

    [ComVisible(true)]
    [Guid("AC32FA59-1CD2-476A-815B-70B5E3FD4DC1")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IParamChangedEventArgs
    {
        FilterParam FilterParam { get; }

        object OldValue { get; }
    }

    [ComVisible(true)]
    [Guid("3C4FB8DF-04A8-453C-AE2C-8CA0925368E0")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IParamChangedEventArgs))]
    public class ParamChangedEventArgs : EventArgs, IParamChangedEventArgs
    {
        public ParamChangedEventArgs(FilterParam param, object oldValue)
        {
            filterparam = param;
            oldvalue = oldValue;

        }

        // Summary:
        //     Gets the name of the property that changed.
        //
        // Returns:
        //     The name of the property that changed.

        private FilterParam filterparam;
        public FilterParam FilterParam { get { return filterparam; } }

        object oldvalue;

        [DispId(0)]
        public object OldValue { get { return oldvalue; } }

    }

    public delegate void ParamToChangeEventHandler(object sender, ParamToChangeEventArgs args);
    public delegate void ParamChangedEventHandler(object sender, ParamChangedEventArgs args);

    // key points about exposing events to com:
    // set up a separate interface
    // it MUST be IDispatch, not Dual
    // its members are functions representing the event handlers - ie matching the event delegate
    // The Class exposing the events uses ComSourceInterfaces to refer to the event interface
    // The Class exposing the events does NOT have to implement this interface
    // The Class exposing the events has a public Event matching the Name of the method in the source interface
    // The class event delegate type macthes the signature in the source class

    [ComVisible(true)]
    [Guid("85DE87CB-3C90-4E6B-904A-54CFF39E9BA4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]              // idispatch, not dual for this
    public interface IFilterParamEvents
    {
        void ParamToChange(object sender, ParamToChangeEventArgs args);
        void ParamChanged(object sender, ParamChangedEventArgs args);
    }



    [ComVisible(true)]
    [Guid("DBEA0F02-284F-4A1C-BE20-856A0FD7730C")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IFilterParam))]
    [ComSourceInterfaces(typeof(IFilterParamEvents))]
    public class FilterParam : IFilterParam, System.ComponentModel.INotifyPropertyChanged
    {
        internal static FilterParam Create(string name, Type datatype)
        {
            if (datatype == typeof(string))
                return new StringFilterParam(name);

            if (datatype == typeof(int))
                return new IntFilterParam(name);

            if (datatype == typeof(bool))
                return new BoolFilterParam(name);

            if (datatype == typeof(DateTime))
                return new DateFilterParam(name);

            if (datatype == typeof(Decimal))
                return new DecimalFilterParam(name);
            if (datatype == typeof(XDocument))
                return new XDocumentFilterParam(name);
            return null;
        }

        protected FilterParam(string name)
        {
            name_ = name;
            Locked = false;
        }
        public void Clear()
        {
            // repeating all this becuase of problems with overrides
            baseValue = null;
        }
        private object value_ = null;
        public virtual object Value
        {
            get { return baseValue; }
            set { baseValue = value; }
        }
        public override string ToString()
        {
            if (Value == null)
                return "NULL";

            return Convert.ToString(Value);
        }
        object baseValue
        {
            get
            {
                return value_;
            }
            set
            {
                if (!Locked)
                {
                    if (value_ != value)
                    {
                        if (NotifyParamToChange(value))
                        {
                            value_ = value;
                            NotifyPropertyChanged("Value");
                        }
                    }
                }
            }
        }
        public virtual void Set(object newvalue)
        {
            Value = newvalue;
        }
        private string name_;
        public string Name
        {
            get
            {
                return name_;
            }
        }
        bool locked_ = false;
        public bool Locked
        {
            get { return locked_; }
            set
            {
                if (locked_ != value)
                {
                    locked_ = value;
                    NotifyPropertyChanged("Locked");
                }
            }
        }

        public event ParamToChangeEventHandler ParamToChange;
        public event ParamChangedEventHandler ParamChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        private bool NotifyParamToChange(object newValue)
        {
            if (ParamToChange != null)
            {
                ParamToChangeEventArgs args = new ParamToChangeEventArgs(this, newValue);
                ParamToChange(this, args);
                return !args.cancel;
            }
            return true;
        }

        private void NotifyParamChanged(object oldValue)
        {
            if (ParamChanged != null)
            {
                ParamChangedEventArgs args = new ParamChangedEventArgs(this, oldValue);
                ParamChanged(this, args);
            }
        }

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

    public class StringFilterParam : FilterParam
    {

        internal StringFilterParam(string name) : base(name) { }

        public override string ToString()
        {
            if (Value == null)
                return base.ToString();
            return string.Format("'{0}'", Value);
        }
        public override object Value
        {
            get { return base.Value; }
            set
            {
                if (value is System.DBNull)
                {
                    Clear();
                    return;
                }
                if (value == null)
                {
                    Clear();
                    return;
                }
                if (value.ToString() == String.Empty)
                {
                    Clear();
                    return;
                }

                base.Value = value;
            }
        }

    }

    public class IntFilterParam : FilterParam
    {

        internal IntFilterParam(string name) : base(name) { }

        public override void Set(object newvalue)
        {
            if (newvalue is System.DBNull)
            {
                Clear();
                return;
            }
            if (newvalue == null)
            {
                Clear();
                return;
            }
            if (newvalue is int)
            {
                Value = newvalue;
            }
        }

    }

    public class DateFilterParam : FilterParam
    {

        internal DateFilterParam(string name) : base(name) { }

        public override void Set(object newvalue)
        {
            if (newvalue is System.DBNull)
            {
                Clear();
                return;
            }
            if (newvalue == null)
            {
                Clear();
                return;
            }
            if (newvalue is DateTime)
            {
                Value = newvalue;
                return;
            }
            DateTime dt_;
            if (DateTime.TryParse(Convert.ToString(newvalue), out dt_))
                Value = dt_;

        }

    }

    public class BoolFilterParam : FilterParam
    {

        internal BoolFilterParam(string name) : base(name) { }

        public override void Set(object newvalue)
        {
            if (newvalue == null)
            {
                Clear();
                return;
            }
            if (newvalue is System.DBNull)
            {
                Clear();
                return;
            }

            if (newvalue is bool)
            {
                Value = newvalue;
                return;
            }
            if (newvalue is int)
            {
                Value = ((int)newvalue == 0 ? false : true);
                return;
            }
            bool f;
            if (bool.TryParse(Convert.ToString(newvalue), out f))
                Value = f;

        }


        public override string ToString()
        {
            if (Value == null)
                return base.ToString();
            return ((bool)Value ? "1" : "0");
        }

    }
    #endregion

    public class DecimalFilterParam : FilterParam
    {

        internal DecimalFilterParam(string name) : base(name) { }

        public override void Set(object newvalue)
        {
            if (newvalue is System.DBNull)
            {
                Clear();
                return;
            }
            if (newvalue == null)
            {
                Clear();
                return;
            }

            if (newvalue is Decimal)
            {
                Value = newvalue;
                return;
            }
            Decimal outval;
            if (Decimal.TryParse(newvalue.ToString(), out outval))
            {
                Value = outval;
            }



        }

    }

    public class XDocumentFilterParam : FilterParam
    {

        internal XDocumentFilterParam(string name) : base(name) { }

        public override void Set(object newvalue)
        {
            if (newvalue is System.DBNull)
            {
                Clear();
                return;
            }
            if (newvalue == null)
            {
                Clear();
                return;
            }

            if (newvalue is Decimal)
            {
                Value = newvalue;
                return;
            }
            Value = XDocument.Parse(newvalue.ToString());

        }

    }




    [ComVisible(true)]
    [Guid("62754C3B-DCCF-4F0D-8861-AF5BE894F7E5")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]

    public interface IDataResult
    {
        int FirstRec { get; }
        bool HasPageInfo { get; }
        int LastRec { get; }
        int NumResults { get; }
        int PageSize { get; }
        int PageNo { get; }
        int LastPage { get; }
        bool IsLastPage { get; }
        string Tag { get; }
        object ResultSet { get; }
    }

    [Guid("AAB85496-E856-451F-B929-6314C2AB5306")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(IDataResult))]
    [ProgId("ScholarSession.DataResult")]
    public class DataResult : IDataResult
    {
        private int numresults_;
        private int firstrec_;
        private int lastrec_;
        private int pagesize_;
        private int pageno_;
        private string tag_;
        object resultSet;


        public DataResult(adoNet.DataSet ds)
        {

            if (ds.Tables.Count > 1)
            {
                bool pageInfo = false;
                adoNet.DataTable summ = ds.Tables[ds.Tables.Count - 1];
                pageInfo = (summ.Columns[0].ColumnName.ToLower() == "nummatches");
                dr(ds, pageInfo, null, false);
            }


            else
                resultSet = ds.Tables[0];
        }

        /// <summary>
        /// Support linq sources
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="hasPageInfo"></param>
        public DataResult(IEnumerable<object> results, PagedResultSummary summ)
        {
            ie(results, summ, null);
        }
        public DataResult(IEnumerable<object> results, PagedResultSummary summ, string tag)
        {
            ie(results, summ, tag);
        }
        public static DataResult FromEFQuery(IEnumerable<object> results, string tag)
        {
            PagedResultSummary summ = new PagedResultSummary();
            return new DataResult(results, summ, tag);
        }
        private DataResult(object result, string tag)
        {
            hasPageInfo = false;
            numresults_ = (result == null ? 0 : 1);
            resultSet = result;
            tag_ = tag;
        }
        public static DataResult FromResult(object result, string tag)
        {
            
          
            return new DataResult(result, tag);
        }
        /// <summary>
        /// Support linq sources
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="hasPageInfo"></param>
        private void ie(IEnumerable<object> results, PagedResultSummary summ, string tag)
        {
            resultSet = results.ToArray();
            numresults_ = summ.NumMatches;
            firstrec_ = (summ.PageFirst == null ? 0 : (int)summ.PageFirst);
            lastrec_ = (summ.PageLast == null ? 0 : (int)summ.PageLast);
            pageno_ = summ.PageNo;
            pagesize_ = summ.PageSize;
            tag_ = tag;
        }

        public DataResult(adoNet.DataSet ds, bool hasPageInfo, string tag, bool namedtables)
        {
            dr(ds, hasPageInfo, tag, namedtables);
        }
        public DataResult(adoNet.DataSet ds, bool hasPageInfo, string tag)
        {
            dr(ds, hasPageInfo, tag, false);
        }
        public DataResult(adoNet.DataSet ds, bool hasPageInfo)
        {
            dr(ds, hasPageInfo, null, false);
        }

        private void dr(adoNet.DataSet ds, bool pageInfo, string tag, bool namedtables)
        {
            tag_ = tag;
            if (pageInfo && (ds.Tables.Count > 1))
            {
                hasPageInfo = pageInfo;

                adoNet.DataTable summ = ds.Tables[ds.Tables.Count - 1];
                if (summ != null)
                {
                    numresults_ = (int)summ.Rows[0][0];
                    if (numresults_ > 0)
                    {
                        firstrec_ = (int)summ.Rows[0][1];
                        lastrec_ = (int)summ.Rows[0][2];
                    }
                    else
                    {
                        firstrec_ = 0;
                        lastrec_ = 0;
                    }
                    pagesize_ = (int)summ.Rows[0][3];
                    pageno_ = (int)summ.Rows[0][4];
                    //remove summary and return what is left
                    ds.Tables.RemoveAt(ds.Tables.Count - 1);
                }
                else
                {
                    this.hasPageInfo = false;
                }

            }
            if (ds.Tables.Count > 1)
                if (namedtables)
                    resultSet = ds;
                else
                    resultSet = ds.Tables;
            else
            {
                if (ds.Tables.Count == 1)
                    resultSet = ds.Tables[0];

            }



        }

        // for preconstructed objects
        public DataResult(Dictionary<string, object> result, string tag)
        {
            resultSet = result;
            this.hasPageInfo = false;
            tag_ = tag;
        }

        public DataResult(Dictionary<string, object> result, bool hasPageInfo, string tag)
        {
            resultSet = result;
            this.hasPageInfo = hasPageInfo;
            tag_ = tag;
        }
        public DataResult(ADODB.Recordset rs)
        {
            resultSet = rs;
        }

        public DataResult(ADODB.Recordset rs, bool hasPageInfo) : this(rs, hasPageInfo, null)
        {
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        public DataResult(ADODB.Recordset rs, bool hasPageInfo, string tag)
        {
            resultSet = rs;
            this.hasPageInfo = hasPageInfo;
            tag_ = tag;
            if (hasPageInfo)
            {

                object recordsAffected = null;          // never seen this before!
                ADODB.Recordset rssummary = rs.NextRecordset(out recordsAffected);

                if (rssummary != null)
                {
                    numresults_ = (int)rssummary.Fields[0].Value;
                    if (numresults_ > 0)
                    {
                        firstrec_ = (int)rssummary.Fields[1].Value;
                        lastrec_ = (int)rssummary.Fields[2].Value;

                    }
                    else
                    {
                        firstrec_ = 0;
                        lastrec_ = 0;
                    }
                    pagesize_ = (int)rssummary.Fields[3].Value;
                    pageno_ = (int)rssummary.Fields[4].Value;
                }
                else
                {
                    this.hasPageInfo = false;
                    numresults_ = -1;
                    firstrec_ = -1;
                    lastrec_ = -1;
                }
                if (!HasPageInfo)
                {
                    numresults_ = -1;
                    firstrec_ = -1;
                    lastrec_ = -1;
                    pagesize_ = 0;
                    pageno_ = 1;

                }
            }
        }

        public object ResultSet
        {
            get { return resultSet; }

        }
        bool hasPageInfo = false;
        public bool HasPageInfo
        {
            get { return hasPageInfo; }
        }

        public int NumResults
        {
            get { return numresults_; }
        }

        public int FirstRec
        {
            get { return firstrec_; }
        }

        public int LastRec
        {
            get { return lastrec_; }
        }

        public int PageSize
        {
            get { return pagesize_; }
        }

        public int PageNo
        {
            get { return pageno_; }
        }

        public bool IsLastPage
        {
            get
            {
                if (HasPageInfo)
                    return (PageNo == LastPage ? true : false);
                return true;
            }
        }
        public int LastPage
        {
            get
            {
                if (PageSize == 0)
                    return 1;

                double lastPage = (double)(NumResults) / PageSize;
                if (lastPage == Math.Floor(lastPage))
                    return Convert.ToInt32(Math.Floor(lastPage));
                else
                    return Convert.ToInt32(Math.Floor(lastPage)) + 1;
            }

        }

        public string Tag
        {
            get { return tag_; }
        }



    }

    #region IDbDefn interface

    public interface IDBDefn
    {
        string Server { get; }
        string Database { get; }
    }
    #endregion

    public class DomainObject : INotifyPropertyChanged, IDBDefn
    {
        protected DomainObject()
        {
            server = String.Empty;
            database = string.Empty;
        }

        protected DomainObject(string server, string database) : this()
        {
            this.server = server;
            this.database = database;
        }

        private string server { get; set; }
        private string database { get; set; }

        string IDBDefn.Server { get { return server; } }
        string IDBDefn.Database { get { return database; } }

        // notification
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

    #region Filter base class


    public class Filter : INotifyPropertyChanged    // , IDBDefn
    {

        private IDataResult dr;

        protected Filter()
        {
            PageNo = 1;
            SortDesc = false;
            ColumnSet = 0;
            PageSize = 50;

            //server = String.Empty;
            //database = string.Empty;

            dic_ = new Dictionary<string, FilterParam>();
        }

        protected Filter(string server, string database) : this()
        {
            //this.server = server;
            //this.database = database;
        }

        //private string server { get; set; }
        //private string database { get; set; }

        //string IDBDefn.Server { get { return server; } }
        //string IDBDefn.Database { get { return database; } }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public string SortColumn { get; set; }
		private bool _sortDesc = false;
        public bool SortDesc
		{
			get
			{
				return _sortDesc;
			}
			set
			{
				_sortDesc = value;
			}
		}
		private string _sortDirection = "asc";
		public string SortDirection
		{
			get
			{
				return _sortDirection;
			}
			set
			{
				_sortDirection = value;
				_sortDesc = (_sortDirection == "desc" ? true : false);
			}
		}
		public int ColumnSet { get; set; }

        private Dictionary<string, FilterParam> dic_ = null;
        protected Dictionary<string, FilterParam> dic
        {
            get
            {
                return dic_;
            }
        }

        protected IFilterParam getParam(string paramName, Type datatype)
        {
            FilterParam fltrp = null;
            if (dic.TryGetValue(paramName, out fltrp))
                return fltrp;
            fltrp = FilterParam.Create(paramName, datatype);
            dic.Add(paramName, fltrp);
            return fltrp;
        }
        protected IFilterParam getParam(string paramName, Type datatype, ParamToChangeEventHandler evt)
        {
            FilterParam fltrp = null;
            if (dic.TryGetValue(paramName, out fltrp))
                return fltrp;
            fltrp = FilterParam.Create(paramName, datatype);
            if (evt != null)
                fltrp.ParamToChange += evt;
            dic.Add(paramName, fltrp);
            return fltrp;
        }

        public int NumResults
        {
            get
            {
                if (dr != null)
                    return dr.NumResults;
                return -1;

            }
        }

        public int FirstRec
        {
            get
            {
                if (dr != null)
                    return dr.FirstRec;
                return -1;
            }
        }

        public int LastRec
        {
            get
            {
                if (dr != null)
                    return dr.LastRec;
                return -1;
            }
        }

        public virtual IDataResult Search()
        {

            return null;
        }

        public IDataResult SearchResult
        {
            get { return dr; }

            protected set
            {
                dr = value;
                NotifyPropertyChanged("SearchResult");
            }
        }




        public void Reset()
        {
            foreach (KeyValuePair<string, FilterParam> fltrp in dic)
            {
                FilterParam prm = fltrp.Value;
                if (!prm.Locked)
                    prm.Clear();
            }
            SearchResult = null;


        }

        public bool FilterSpecified
        {
            get
            {
                foreach (KeyValuePair<string, FilterParam> fltrp in dic)
                {
                    if (fltrp.Value.Value != null)
                        return true;
                }
                return false;
            }
        }
        // page management

        public void SetPage(int newPageNo)
        {
            if (SearchResult == null)
                return;
            if (newPageNo < 1)
                return;
            if (newPageNo > SearchResult.LastPage)
                return;
            PageNo = newPageNo;
            Search();
        }
        public void NextPage()
        {
            SetPage(PageNo + 1);
        }
        public void PrevPage()
        {
            SetPage(PageNo - 1);
        }
        public string xmlFilter()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Filter/>");
            foreach (KeyValuePair<string, FilterParam> fltrp in dic)
            {
                IFilterParam prm = fltrp.Value;
                if (prm != null && prm.Value != null)
                    doc.DocumentElement.SetAttribute(prm.Name, Convert.ToString(prm.Value));
            }
            return doc.OuterXml;
        }

        public string SSRSParams()
        {
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, FilterParam> fltrp in dic)
            {
                IFilterParam prm = fltrp.Value;
                if (prm.Value != null)
                    sb.Append(String.Format("&{0}={1}", prm.Name, Convert.ToString(prm.Value)));
            }
            string outstr = sb.ToString();
            if (outstr.Length > 0)
                outstr = outstr.Substring(1);
            return outstr;                  // no leading &
        }

        // notification
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        ////public virtual string getErrorInfo(string propertyname)
        ////{

        ////}
        public void FromJSON(Newtonsoft.Json.Linq.JObject jobj)
        {
            foreach (var prop in jobj)
            {
                // movement of obects up the wire creates
                // a conflict between javascript naming standards (properties in camelCase)
                // and C# conventions (properties in PascalCase)
                // to ameliorate this somewhat we can translate here
                string propname = prop.Key.ToLower();
                switch (propname)
                {
                    case "pageno":
                        PageNo = (int)prop.Value;
                        break;
                    case "pagesize":
                        PageSize = (int)prop.Value;
                        break;
                    case "sortcolumn":
                        SortColumn = (string)prop.Value;
                        break;
                    case "sortdir":
                    case "sortdirection":           // accommodate api change on the client side
                        SortDesc = (((string)prop.Value) == "desc"); //?true:false);
                        break;
                    case "sortdesc":
                        SortDesc = (bool)prop.Value;
                        break;
                    case "columnset":
                        ColumnSet = (int)prop.Value;
                        break;
                    default:
                        IFilterParam param = this.GetType().GetProperty(prop.Key).GetValue(this, null) as IFilterParam;
                        if (prop.Value.Type == Newtonsoft.Json.Linq.JTokenType.Null)
                            param.Clear();
                        else
                        {
                            if (param is Softwords.DataTools.StringFilterParam)
                            {
                                param.Value = prop.Value.ToString();
                                break;
                            }
                            if (param is Softwords.DataTools.IntFilterParam)
                            {
                                param.Value = Convert.ToInt32(prop.Value);
                                break;
                            }
                        };
                        break;
                }
            }
        }
    }
    #endregion


    /// <summary>
    /// PagedResultSummary is a helper cfalss repreting the summary returned as the second resultset from
    /// xxxFilterPaged stored procs
    /// linq methods can cast the second of Multiple results to IEnumerable<PagedResultsummary>
    /// this makes it simple to create a DataResult from a linq source
    /// </summary>
    public class PagedResultSummary
    {
        public int NumMatches;
        public int? PageFirst;
        public int? PageLast;
        public int PageSize;
        public int PageNo;
        public int ColumnSet;
    }

}