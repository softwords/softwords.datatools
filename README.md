* ADO Net tools for data management. Provides the abstraction IDataResult 
and tools for manipulating the DataResult implementation of this interface; used widely in Pacific EMIS.
